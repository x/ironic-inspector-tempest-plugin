This project is no longer maintained.

In fact, it didn't ever exist; it was created with a .gitreview
and that's it.

The intent was for it to be used for ironic-inspector, but both
ironic and ironic-inspector use ironic-tempest-plugin, so this
wasn't ever used.
